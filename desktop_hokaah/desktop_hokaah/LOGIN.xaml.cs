﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace desktop_hokaah
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
 
    {
        public string usuario, senha;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TxtLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtLogin.Text == "")
                {
                    MessageBox.Show("Preencher o campo usuário corretamente!!!", "Login do Usuário");
                }
                else
                {
                    usuario = txtLogin.Text;
                    txtSenha.IsEnabled = true;
                    txtSenha.Focus();
                }
            }
        }

        private void TxtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (e.Key == Key.Enter)
                {
                    if (txtSenha.Password == "")
                    {
                        MessageBox.Show("Preencher o campo senha corretamente!!!", "Senha do Usuário");
                    }
                    else
                    {
                        senha = txtSenha.Password;
                        BtnEntrar.IsEnabled = true;
                        BtnEntrar.Focus();
                    }
                }
            }
        }

        private void BtnCadastro_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnEntrar_Click(object sender, RoutedEventArgs e)
        {
            if (usuario == "ADMIN" && senha == "1234")
            {
                Hide(); //Esconder
                pag_inicial frm = new pag_inicial();
                frm.Show();
            }
            else if (usuario == "TESTE" && senha == "5678")
            {
                Hide();
                pag_inicial frm = new pag_inicial();
                frm.Show();
            }
            else
            {
                MessageBox.Show("Favor preencher o usuário ou a senha corretamente!!!");
                txtLogin.Clear();
                txtSenha.Clear();
                txtLogin.Focus();
                txtSenha.IsEnabled = false;
                BtnEntrar.IsEnabled = false;
            }
        }
    }
    }
}
