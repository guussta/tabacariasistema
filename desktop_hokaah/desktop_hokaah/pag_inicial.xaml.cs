﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace desktop_hokaah
{
    /// <summary>
    /// Lógica interna para pag_inicial.xaml
    /// </summary>
    public partial class pag_inicial : Window
    {
        public pag_inicial()
        {
            InitializeComponent();
        }

        private void Bt_home_Click(object sender, RoutedEventArgs e)
        {
            Hide(); //Esconder Atual
            pag_inicial inicio = new pag_inicial();
            inicio.Show();
        }

        private void Bt_reserva_Click(object sender, RoutedEventArgs e)
        {
            Hide(); //Esconder Atual
             pgReserva = new pgReserva();
            pgReserva.Show();
        }

        private void Bt_eventos_Click(object sender, RoutedEventArgs e)
        {

            Hide(); //Esconder Atual
            EVENTOS = new EVENTOS();
            EVENTOS.Show();
        }

        private void Bt_estoque_Click(object sender, RoutedEventArgs e)
        {
            Hide(); //Esconder Atual
            ESTOQUE = new ESTOQUE();
            ESTOQUE.Show();
        }
    }
}
